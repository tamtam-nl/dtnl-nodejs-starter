import { Node } from 'http/example/node.entity';
import { MigrationInterface, QueryRunner } from 'typeorm';

export class Node1663059292900 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.manager.save(
      queryRunner.manager.create<Node>(Node, {
        name: 'test',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE * FROM node`);
  }
}
