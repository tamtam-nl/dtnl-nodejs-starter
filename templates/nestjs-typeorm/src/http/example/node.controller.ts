import {
  Get,
  Controller,
  Param,
  ParseIntPipe,
  Post,
  Body,
  Delete,
  NotFoundException,
  Put,
} from '@nestjs/common';
import { NodeService } from './node.service';
import { Node } from './node.entity';
import { CreateNodeDto } from './dto/create-node.dto';
import { UpdateNodeDto } from './dto/update-node.dto';
import { Response } from 'util/response';

@Controller('/examples')
export class NodeController {
  constructor(
    private readonly nodeService: NodeService,
    private readonly response: Response,
  ) {}

  @Get(':id')
  async find(@Param('id', ParseIntPipe) id: number) {
    const node = await this.nodeService.find(id);
    return this.response.format({ id }, node);
  }

  @Get()
  async findAll() {
    const nodes: Node[] = await this.nodeService.findAll();
    return this.response.format('none', nodes);
  }

  @Post()
  async create(@Body() data: CreateNodeDto): Promise<Node> {
    const createdNode: Node = await this.nodeService.create(data);
    return this.response.format({ data }, createdNode);
  }

  @Put(':id')
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() data: UpdateNodeDto,
  ): Promise<Node> {
    const node = await this.nodeService.find(id);
    if (!node) throw new NotFoundException();

    const updatedNode: Node = await this.nodeService.update(id, data);
    return this.response.format({ id, data }, updatedNode);
  }

  @Delete(':id')
  async delete(@Param('id', ParseIntPipe) id: number): Promise<Node> {
    const node = await this.nodeService.find(id);
    if (!node) throw new NotFoundException();

    const deletedNode: Node = await this.nodeService.delete(id);
    return this.response.format({ id }, deletedNode);
  }
}
