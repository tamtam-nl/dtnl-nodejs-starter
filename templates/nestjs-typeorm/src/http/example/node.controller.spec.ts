import { Test, TestingModule } from '@nestjs/testing';
import { CreateNodeDto } from './dto/create-node.dto';
import { NodeController } from './node.controller';
import { NodeService } from './node.service';
import { Response } from 'util/response';

const createNodeDto: CreateNodeDto = {
  id: 1,
  name: 'name #1',
};

describe('NodeController', () => {
  let nodeController: NodeController;
  let nodeService: NodeService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [NodeController],
      providers: [
        NodeService,
        Response,
        {
          provide: NodeService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation((node: CreateNodeDto) =>
                Promise.resolve({ id: 1, ...node }),
              ),
            findAll: jest.fn().mockResolvedValue([
              {
                id: 1,
                name: 'name #1',
              },
              {
                id: 2,
                name: 'name #2',
              },
            ]),
            find: jest.fn().mockImplementation((id: number) =>
              Promise.resolve({
                name: 'name #1',
                id,
              }),
            ),
            delete: jest.fn(),
          },
        },
      ],
    }).compile();

    nodeController = app.get<NodeController>(NodeController);
    nodeService = app.get<NodeService>(NodeService);
  });

  it('should be defined', () => {
    expect(nodeController).toBeDefined();
  });

  describe('create()', () => {
    it('should create a node', () => {
      nodeController.create(createNodeDto);
      expect(nodeService.create).toHaveBeenCalledWith(createNodeDto);
    });
  });

  describe('findAll()', () => {
    it('should find all nodes ', () => {
      nodeController.findAll();
      expect(nodeService.findAll).toHaveBeenCalled();
    });
  });

  describe('find()', () => {
    it('should find a node', () => {
      nodeController.find(1);
      expect(nodeService.find).toHaveBeenCalled();
    });
  });
});
