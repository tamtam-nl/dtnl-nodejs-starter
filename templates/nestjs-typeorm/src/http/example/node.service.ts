import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Node } from './node.entity';

@Injectable()
export class NodeService {
  constructor(
    @InjectRepository(Node)
    private nodeRepository: Repository<Node>,
  ) {}

  findAll(): Promise<Node[]> {
    return this.nodeRepository.find();
  }

  find(id: number): Promise<Node> {
    return this.nodeRepository.findOneBy({ id });
  }

  create(dtoData: object): Promise<Node> {
    return this.nodeRepository.save(dtoData);
  }

  async update(id: number, attributes: object): Promise<Node> {
    await this.nodeRepository.update(id, attributes);
    return this.find(id);
  }

  async delete(id: number): Promise<Node> {
    const node = this.find(id);
    await this.nodeRepository.delete(id);
    return node;
  }
}
