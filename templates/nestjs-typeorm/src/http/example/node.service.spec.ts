import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Node } from './node.entity';
import { NodeService } from './node.service';
import { Repository } from 'typeorm';

const nodeArray = [
  {
    id: 1,
    name: 'name #1',
  },
  {
    id: 2,
    name: 'name #2',
  },
];

const oneNode = {
  id: 1,
  name: 'name #1',
};

describe('NodeService', () => {
  let service: NodeService;
  let repository: Repository<Node>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NodeService,
        {
          provide: getRepositoryToken(Node),
          useValue: {
            find: jest.fn().mockResolvedValue(nodeArray),
            findOneBy: jest.fn().mockResolvedValue(oneNode),
            save: jest.fn().mockResolvedValue(oneNode),
            remove: jest.fn(),
            delete: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<NodeService>(NodeService);
    repository = module.get<Repository<Node>>(getRepositoryToken(Node));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('create()', () => {
    it('should successfully insert a node', () => {
      const oneNode = {
        id: 1,
        name: 'name #1',
      };

      expect(
        service.create({
          id: 1,
          name: 'name #1',
        }),
      ).resolves.toEqual(oneNode);
    });
  });

  describe('findAll()', () => {
    it('should return an array of nodes', async () => {
      const nodes = await service.findAll();
      expect(nodes).toEqual(nodeArray);
    });
  });

  describe('findOne()', () => {
    it('should get a single node', () => {
      const repoSpy = jest.spyOn(repository, 'findOneBy');
      expect(service.find(1)).resolves.toEqual(oneNode);
      expect(repoSpy).toBeCalledWith({ id: 1 });
    });
  });

  describe('remove()', () => {
    it('should call remove with the passed value', async () => {
      const removeSpy = jest.spyOn(repository, 'delete');
      const retVal = await service.delete(2);
      expect(removeSpy).toBeCalledWith(2);
      expect(retVal).toBe(oneNode);
    });
  });
});
