import { Module } from '@nestjs/common';
import { NodeController } from './node.controller';
import { NodeService } from './node.service';
import { Node } from './node.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Response } from 'util/response';

@Module({
  imports: [TypeOrmModule.forFeature([Node])],
  controllers: [NodeController],
  providers: [NodeService, Response],
})
export class NodeModule {}
