import { IsString, MaxLength } from 'class-validator';

export class UpdateNodeDto {
  @IsString()
  @MaxLength(60)
  name: string;
}
