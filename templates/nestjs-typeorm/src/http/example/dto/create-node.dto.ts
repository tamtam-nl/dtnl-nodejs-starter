import { IsNumber, IsString, MaxLength } from 'class-validator';

export class CreateNodeDto {
  @IsNumber()
  id: number;

  @IsString()
  @MaxLength(60)
  name: string;
}
