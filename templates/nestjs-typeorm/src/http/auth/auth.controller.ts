import { Get, Controller, UseGuards, Post, Body } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('tokens')
  async createToken(): Promise<any> {
    return this.authService.createToken({ user: 'example' });
  }

  @Post('refresh-token')
  async refreshToken(@Body() request): Promise<object> {
    return this.authService.refreshToken(request);
  }

  @Get('data')
  @UseGuards(AuthGuard('jwt'))
  findAll() {
    console.log('Guarded endpoint');
  }
}
