import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  validateUser(payload) {
    // insert user comparing logic here, for example purposes hard coded
    return { user: 'example' };
  }

  createToken(payload: any) {
    return this.jwtService.sign(payload);
  }

  async refreshToken(request: any) {
    const name = await this.jwtService.decode(request.token);
    const token = this.jwtService.sign({ name });

    return {
      token,
    };
  }
}
