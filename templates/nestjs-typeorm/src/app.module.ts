import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from 'common/jwt/jwt.strategy';
import { NodeModule } from 'http/example/node.module';
import { Node } from 'http/example/node.entity';
import { AuthController } from 'http/auth/auth.controller';
import { AuthService } from 'http/auth/auth.service';
import { load } from 'config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      ignoreEnvFile: true, // the env file is already loaded in main.ts
      load,
    }),
    TypeOrmModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        migrationsTableName: 'custom_migration_table',
        type: 'mysql',
        host: configService.get('db.host'),
        port: parseInt(configService.get('db.port')),
        username: configService.get('db.username'),
        password: configService.get('db.password'),
        database: configService.get('db.name'),
        entities: [Node],
        migrations: ['migration/*.ts'],
        synchronize: false,
      }),
      inject: [ConfigService],
    }),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: {
        expiresIn: process.env.JWT_EXPIRE_TIME,
      },
    }),
    NodeModule,
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, ConfigService],
})
export class AppModule {}
