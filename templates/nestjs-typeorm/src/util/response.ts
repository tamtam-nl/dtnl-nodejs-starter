import { Injectable } from '@nestjs/common';

@Injectable()
export class Response {
  format(metadata, results) {
    const resultResponse: any = {
      metadata: metadata,
      results: results,
    };

    return resultResponse;
  }
}
