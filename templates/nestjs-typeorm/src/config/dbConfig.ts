export const dbConfig = {
  host: process.env.DB_HOST || 'localhost',
  username: process.env.DB_USERNAME || 'root',
  password: process.env.DB_PASSWORD || 'admin',
  name: process.env.DB_NAME || 'test',
  port: process.env.DB_PORT || 3306,
  synchronize: process.env.DB_SYNCHRONIZE || true,
  logging: process.env.DB_LOGGING || true,
};
