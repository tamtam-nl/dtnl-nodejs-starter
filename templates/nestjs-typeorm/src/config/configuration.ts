import { registerAs } from '@nestjs/config';

import { appConfig } from './appconfig';
import { dbConfig } from './dbConfig';

export const load = [
  registerAs('app', () => appConfig),
  registerAs('db', () => dbConfig),
];
