/**
 * This is the entrypoint for your application.
 *
 * Update this docblock with some useful information, like the
 * name of the project, authors, etc., or any other useful
 * documentation.
 */

import express        from 'express';

import Middleware from './middleware/example.middleware';
import ExampleRouter  from './routes/example.route';
import Config from './config/example.config';

class App{
  public app: express.Application;
  public middleware: Middleware;
  public config: Config;

  constructor(){
    this.app = express();

    this.middleware = new Middleware(this.app);
    this.config = new Config();
    this.middleware.initializeMiddleware();
    this.initializeRoutes();
  }


  public getServer(){
    return this.app;
  }


  private initializeRoutes(){
    const exampleRouter = new ExampleRouter();
    this.app.use('/' , exampleRouter.router )
  }



  public listen() {
    this.app.listen(this.config.port, () => {
      console.info(`=================================`);
      console.info(`🚀 App listening on the port ${this.config.port}`);
      console.info(`=================================`);
    });
  }
}

const app = new App();
app.listen();


export default App;