import { Router } from 'express';
import ExampleController from '../controllers/example.controller';

class ExampleRouter {
    private _path = '/';
    private _router = Router();
    private _exampleController = new ExampleController();

    constructor(){
        this.initializeRoutes();
    }

    private initializeRoutes(){
        this._router.get(`${this._path}`, this._exampleController.exampleMethod);
    }

    public get router(){
        return this._router;
    }
    
}

export default ExampleRouter;