import { Examples } from "../interfaces/example.interface";


class ExampleService {

    private _example: Examples;

    constructor(){
        this._example = {
            id: 1,
        }
    }

    public get example(){
        return this._example;
    }

    public method(){
        //Do something
    }
}

export default ExampleService;