import dotenv from 'dotenv';

class Config {
    private _port: number;

    constructor(){
        dotenv.config({ 
            path: __dirname + `/../../.env.${process.env.NODE_ENV}`,
            debug: Boolean(process.env.DEBUG) 
        });

        this._port = Number(process.env.PORT);
    }

    public get port(){
        return this._port;
    }
}

export default Config;