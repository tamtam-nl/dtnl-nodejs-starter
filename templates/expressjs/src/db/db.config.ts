import { Schema, model, connect } from 'mongoose';
import { IUser } from './db.interface';


class dbConfig {

    private userSchema: Schema<IUser>;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    private User: any;

    constructor() {
        this.userSchema = new Schema<IUser>({
            name: { type: String, required: true },
            email: { type: String, required: true },
            avatar: String
          });

        this.User = model<IUser>('User', this.userSchema);

        this.run().catch(err => console.log(err));
    }
  

  public run = async () => {
    await connect('mongodb://localhost:3000/test');
  
    const user = new this.User({
      name: 'Bill',
      email: 'bill@initech.com',
      avatar: 'https://i.imgur.com/dM7Thhn.png'
    });
    await user.save();
  
  } 
}

export default dbConfig



