import express        from 'express';
import cors           from 'cors';
import helmet         from 'helmet';
import morgan from 'morgan';
import cookieParser   from 'cookie-parser';
import swaggerJSDoc   from 'swagger-jsdoc'; 
import swaggerUi      from 'swagger-ui-express';

class Middleware{

    private _app: express.Application;

    constructor(app: express.Application){
        this._app = app;
    }

    public initializeMiddleware(){
        this.initializeMorgan();
        this.initializeCors();
        this.initializeHelmet();
        this.initializeJSON();
        this.initializeCookieParser();
        this.initializeSwagger();
    }

    private initializeMorgan(){
        this._app.use(morgan("combined"));
    }

    private initializeCors(){
        this._app.use(cors());
    }

    private initializeHelmet(){
        this._app.use(helmet());
    }

    private initializeJSON(){
        this._app.use(express.json());
        this._app.use(express.urlencoded({ extended: true }));
    }

    private initializeCookieParser(){
        this._app.use(cookieParser());  
    }

    private initializeSwagger() {
        const options = {
          swaggerDefinition: {
            info: {
              title: 'REST API',
              version: '1.0.0',
              description: 'Example docs',
            },
          },
          apis: ['swagger.yaml'],
        };
    
        const specs = swaggerJSDoc(options);
        this._app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));
      }
}

export default Middleware;