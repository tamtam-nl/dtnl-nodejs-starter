import { NextFunction, Request, Response } from "express";
import { Examples } from "../interfaces/example.interface";
import ExampleService from "../services/example.service"

class exampleController {
    private _exampleService = new ExampleService();

    public exampleMethod = (req: Request, res: Response, next: NextFunction): void => { 
        try {
            const userData: Examples = req.body;
            this._exampleService.method();   
    
            res.status(200).json({ data: userData, message: 'Example' });
          } catch (error) {
            next(error);
          }
    }
}

export default exampleController;