import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './http/app/app.controller';
import { AppService } from './http/app/app.service';
import { load } from './config/configuration';
import { ScheduleModule } from '@nestjs/schedule';
import { OrderController } from './http/order/order.controller';
import { OrderService } from './http/order/order.service';
import { ShopifyService } from './common/services/shopify.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      ignoreEnvFile: true, // the env file is already loaded in main.ts
      load,
    }),
    TypeOrmModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        migrationsTableName: 'migrations',
        type: 'postgres',
        host: configService.get('db.host'),
        port: parseInt(configService.get('db.port')),
        username: configService.get('db.username'),
        password: configService.get('db.password'),
        database: configService.get('db.name'),
        entities: [],
        migrations: ['migrations/*.ts'],
        synchronize: false,
      }),
      inject: [ConfigService],
    }),
    ScheduleModule.forRoot(),
  ],
  controllers: [AppController, OrderController],
  providers: [AppService, ConfigService, OrderService, ShopifyService],
})
export class AppModule {}
