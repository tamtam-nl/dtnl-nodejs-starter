import { Controller, Post } from '@nestjs/common';
import { Body, HttpCode, UseGuards } from '@nestjs/common/decorators';
import { ShopifyService, Order } from 'src/common/services/shopify.service';
import { OrderUpdateWebhookGuard } from './order-update-webhook.guard';

@Controller('orders')
export class OrderController {
  constructor(private readonly shopifyService: ShopifyService) {}

  @Post()
  @UseGuards(OrderUpdateWebhookGuard)
  @HttpCode(200)
  async orderUpdateWebhook(@Body() payload: Order) {}
}
