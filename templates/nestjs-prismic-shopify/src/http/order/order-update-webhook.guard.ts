import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { validateHmac } from 'src/util/webhook';

const {
  SHOPIFY_WEBHOOK_SECRET_KEY
} = process.env;

@Injectable()
export class OrderUpdateWebhookGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    const headers = request.headers;

    const hmacHeader = headers['x-shopify-hmac-sha256'];

    if (!hmacHeader) {
      return false;
    }

    return validateHmac(request.rawBody, hmacHeader, SHOPIFY_WEBHOOK_SECRET_KEY);
  }
}
