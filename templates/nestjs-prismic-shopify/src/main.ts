import * as dotenv from 'dotenv';
dotenv.config(); // must be executed before any other imports

import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { GlobalExceptionFilter } from './common/middleware/exception-handler';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
    rawBody: true,
  });

  const config = new DocumentBuilder()
    .setTitle('Avery Dennison API')
    .setVersion('1.0')
    .build();

  app.setGlobalPrefix('api/' + app.get(ConfigService).get('app.version'));
  app.useGlobalFilters(new GlobalExceptionFilter(app.get(ConfigService)));

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(app.get(ConfigService).get('app.port'));
}
bootstrap();
