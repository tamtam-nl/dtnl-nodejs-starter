import { createHmac } from 'crypto';

// As long as the Shopify one is broken, we use this custom one.
// https://github.com/Shopify/shopify-api-js/issues/275
// https://johnschmidt.de/post/verifying-your-shopify-webhooks-in-serverless-functions
export function validateHmac(rawBody: any, header: string, key: string) {
  return header === createHmac('sha256', key).update(rawBody).digest('base64');
}
