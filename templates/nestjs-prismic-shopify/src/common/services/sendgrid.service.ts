import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MailService } from '@sendgrid/mail';

@Injectable()
export class SendgridService {
  private readonly logger = new Logger(SendgridService.name);
  private mailService: MailService;
  private from = {
    email: this.configService.get('sendgrid.from'),
    name: this.configService.get('sendgrid.fromName'),
  };

  constructor(private configService: ConfigService) {
    this.mailService = new MailService();
    this.mailService.setApiKey(configService.get('sendgrid.apiKey'));
  }

  // change this with a template
  sendMail = async (email: string, subject: string, html: string) => {
    const data = {
      from: this.from,
      html,
      to: email,
      subject: subject,
    };

    this.logger.verbose(`Sending mail from ${this.from.name} <${this.from.email}>`);
    const result = await this.mailService.send(data);
    this.logger.verbose(`Sending mail result ${result[0]?.statusCode}`);
  };
}
