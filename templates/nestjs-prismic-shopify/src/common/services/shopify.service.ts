import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as Shopify from 'shopify-api-node';
import { Order } from '@shopify/shopify-api/rest/admin/2023-01/order';

@Injectable()
export class ShopifyService {
  public shopify: Shopify;

  constructor(private configService: ConfigService) {
    this.shopify = new Shopify({
      accessToken: configService.get('shopify.accessToken'),
      shopName: configService.get('shopify.shopName'),
      apiVersion: configService.get('shopify.apiVersion'),
      autoLimit: configService.get('shopify.autoLimit'),
    });
  }
}

export { Order };
