export const dbConfig = {
  host: process.env.DB_HOST || 'localhost',
  username: process.env.DB_USERNAME || 'postgres',
  password: process.env.DB_PASSWORD || 'admin',
  name: process.env.DB_NAME || 'ad',
  port: process.env.DB_PORT || 5432,
  synchronize: process.env.DB_SYNCHRONIZE || false,
  logging: process.env.DB_LOGGING || true,
};
