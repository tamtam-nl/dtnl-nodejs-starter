import { registerAs } from '@nestjs/config';

import { appConfig } from './app.config';
import { dbConfig } from './db.config';
import { shopifyConfig } from './shopify.config';

export const load = [
  registerAs('app', () => appConfig),
  registerAs('db', () => dbConfig),
  registerAs('shopify', () => shopifyConfig),
];
