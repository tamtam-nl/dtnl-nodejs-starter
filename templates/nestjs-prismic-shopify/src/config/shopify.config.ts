export const shopifyConfig = {
  accessToken: process.env.SHOPIFY_API_ACCESS_TOKEN || '',
  apiVersion: '2023-01',
  autoLimit: true,
  shopName: process.env.SHOPIFY_SHOP_NAME || '',
};
