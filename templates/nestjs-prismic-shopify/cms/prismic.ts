import * as prismicCustomTypes from '@prismicio/custom-types-client';
import fetch from 'node-fetch';

/* eslint-disable */
require('dotenv').config();

export const prismicClient = prismicCustomTypes.createClient({
  repositoryName: process.env.PRISMIC_REPOSITORY_NAME,
  token: process.env.PRISMIC_CUSTOM_TYPES_API_TOKEN,
  fetch,
});
