import { prismicClient } from '../prismic';
import { CustomTypeModelDefinition } from '@prismicio/types';

export async function up() {
  const onlineTrainingFields = {
    photo: {
      type: 'Image',
      config: {
        label: 'Image',
        placeholder: 'Upload an image',
      },
    },
    videoLink: {
      type: 'Link',
      config: {
        label: 'Video Link',
        placeholder: 'Enter video url',
      },
    },
    description: {
      type: 'Text',
      config: {
        label: 'Description',
        placeholder: 'Enter short description',
      },
    },
    keywords: {
      type: 'Group',
      config: {
        label: 'Keywords',
        fields: {
          keyword: {
            type: 'Text',
            config: {
              label: 'Keyword',
              placeholder: 'Enter keyword',
            },
          },
        },
      },
    },
    tags: {
      type: 'Group',
      label: 'Tags',
      config: {
        fields: {
          tag: {
            type: 'Text',
            config: {
              label: 'Tag',
              placeholder: 'Enter tag',
            },
          },
        },
      },
    },
    category: {
      type: 'Text',
      config: {
        label: 'Category',
        placeholder: 'Enter category',
      },
    },
  };
  const onlineTrainingModel = {
    id: 'online-training',
    label: 'Online Training',
    repeatable: true,
    json: {
      Main: onlineTrainingFields,
    } as CustomTypeModelDefinition,
    status: true,
  };
  await prismicClient.insertCustomType(onlineTrainingModel);
}

export async function down() {
  await prismicClient.removeCustomType('online-training');
}
