import { prismicClient } from '../prismic';
import { CustomTypeModelDefinition } from '@prismicio/types';

export async function up() {
  const newsFields = {
    title: {
      type: 'Text',
      config: {
        label: 'Title',
        placeholder: 'Enter title',
      },
    },
    image: {
      type: 'Image',
      config: {
        label: 'Cover Image',
        placeholder: 'Upload an image',
      },
    },
    publishDate: {
      type: 'Date',
      config: {
        label: 'Publish date',
        default: 'now',
      },
    },
    content: {
      type: 'StructuredText',
      config: {
        label: 'News content',
        placeholder: 'Enter news content',
        multi:
          'paragraph, preformatted, heading1, heading2, heading3, heading4, heading5, heading6, strong, em, hyperlink, image, embed, list-item, o-list-item',
      },
    },
  };
  const newsModel = {
    id: 'news',
    label: 'News',
    repeatable: true,
    json: {
      Main: newsFields,
    } as CustomTypeModelDefinition,
    status: true,
  };
  await prismicClient.insertCustomType(newsModel);
}

export async function down() {
  await prismicClient.removeCustomType('news');
}
