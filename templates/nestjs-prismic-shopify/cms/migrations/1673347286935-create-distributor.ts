import { prismicClient } from '../prismic';
import { CustomTypeModelDefinition } from '@prismicio/types';

export async function up() {
  const distributorFields = {
    id: {
      type: 'Number',
      config: {
        label: 'ID',
        placeholder: 'Enter ID',
      },
    },
    name: {
      type: 'Text',
      config: {
        label: 'Name',
        placeholder: 'Enter name',
      },
    },
    legalEntity: {
      type: 'Text',
      config: {
        label: 'Legal Entity',
        placeholder: 'Enter legal entity',
      },
    },
    website: {
      type: 'Text',
      config: {
        label: 'Website',
        placeholder: 'Enter website url',
      },
    },
    phone: {
      type: 'Text',
      config: {
        label: 'Phone',
        placeholder: 'Enter phone',
      },
    },
    fax: {
      type: 'Text',
      config: {
        label: 'Fax',
        placeholder: 'Enter fax',
      },
    },
    country: {
      type: 'Text',
      config: {
        label: 'Country',
        placeholder: 'Enter country',
      },
    },
    city: {
      type: 'Text',
      config: {
        label: 'City',
        placeholder: 'Enter city',
      },
    },
    postalCode: {
      type: 'Text',
      config: {
        label: 'Postal Code',
        placeholder: 'Enter postal code',
      },
    },
    street: {
      type: 'Text',
      config: {
        label: 'Street',
        placeholder: 'Enter street',
      },
    },
    houseNumber: {
      type: 'Text',
      config: {
        label: 'House number',
        placeholder: 'Enter house number',
      },
    },
    sector: {
      type: 'Text',
      config: {
        label: 'Sector',
        placeholder: 'Enter sector',
      },
    },
    materialsGroup: {
      type: 'Number',
      config: {
        label: 'Materials Group',
        placeholder: 'Enter materials group',
      },
    },
    photo: {
      type: 'Image',
      config: {
        label: 'Image',
        placeholder: 'Upload an image',
      },
    },
  };
  const distributorModel = {
    id: 'distributor',
    label: 'Distributor',
    repeatable: true,
    json: {
      Main: distributorFields,
    } as CustomTypeModelDefinition,
    status: true,
  };
  await prismicClient.insertCustomType(distributorModel);
}

export async function down() {
  await prismicClient.removeCustomType('distributor');
}
