# Avery Dennison - API

This is the backend for the Avery Dennison project. It is a REST API, Cronjob host and webhook receiver built with NestJS.

## Installation

Copy the `.env` file from the Test folder in Keeper and place it in the root of the project.

```bash
npm install
```

## Running the app

```bash
# development
npm run start

# watch mode
npm run start:dev

# production mode
npm run start:prod
```

## Running tests

```bash
# unit tests
npm run test

# e2e tests
npm run test:e2e

# test coverage
npm run test:cov
```

## Migrations

Add a new database migration by running the following command:

```bash
npm run typeorm migration:create -n <migration-name>
```
