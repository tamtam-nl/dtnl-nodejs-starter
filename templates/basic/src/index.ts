/**
 * This is the entrypoint for your application.
 *
 * Update this docblock with some useful information, like the
 * name of the project, authors, etc., or any other useful
 * documentation.
 */

import dotenv     from 'dotenv';
import express    from 'express';

const app = express();
const port = 1234;

if (process.env.NODE_ENV !== 'production') {
  dotenv.config({ debug: Boolean(process.env.DEBUG) });
}

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

// export const app = {};