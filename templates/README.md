# Available Templates

- `basic`: Single entry point.
- `expressjs`: Complete setup with controllers, services, database models, and routes.
- `nestjs-mongoose`: Partial setup with mongoose.
- `nestjs-prismic-shopify`: Setup with Prismic (CMS) and Shopify (e-commerce). TypeORM is set up but not in use.
- `nestjs-typeorm`: Complete setup with controllers and database migrations.
