export function getCustomMessage(code): CustomError {
  const filterDictionary = {
    400: new ForbiddenError(),
    401: new UnauthorizedError(),
    403: new ForbiddenError(),
    404: new NotFoundError(),
    500: new InternalServerError(),
    502: new BadGatewayError(),
  };

  return filterDictionary[code];
}

export class CustomError extends Error {
  message: string;
  devMessage: string;
  moreInfo: string;

  constructor(message?: string, devMessage?: string, moreInfo?: string) {
    super(message);
    this.name = this.constructor.name;
    this.devMessage = devMessage;
    this.moreInfo = moreInfo;
    Object.setPrototypeOf(this, new.target.prototype);
  }
}

export class CustomHttpError extends CustomError {
  status: number;
  devMessage: string;
  constructor(
    message?: string,
    devMessage?: string,
    moreInfo?: string,
    status?: number,
  ) {
    super(message, devMessage, moreInfo);
    if (status) {
      this.status = status;
    }
  }
}

export class BadRequestError extends CustomHttpError {
  status = 400;
  constructor(message?: string, moreInfo?: string) {
    super(
      message || 'Bad Request',
      '[The endpoint or file you are trying to access cannot be found or reached, ' +
        'try to change the endpoint and take a good look at the endpoint you are trying to reach]',
      moreInfo,
    );
  }
}

export class ForbiddenError extends CustomHttpError {
  status = 403;
  constructor(message?: string, moreInfo?: string) {
    super(
      message || 'Forbidden',
      '[The endpoint or file you are trying to access is forbidden or unauthorized ' +
        'make sure you are authenticated to access this endpoint and check if the endpoint is protected]',
      moreInfo ||
        'https://blog.airbrake.io/blog/http-errors/403-forbidden-error',
    );
  }
}

export class UnauthorizedError extends ForbiddenError {
  status = 401;
  constructor(message?: string) {
    super(
      message || 'Unauthorized',
      'https://blog.airbrake.io/blog/http-errors/401-unauthorized-error',
    );
  }
}

export class NotFoundError extends BadRequestError {
  status = 404;
  constructor(message?: string) {
    super(
      message || 'Not Found',
      'https://blog.airbrake.io/blog/http-errors/404-not-found-error',
    );
  }
}

export class InternalServerError extends CustomHttpError {
  status = 500;
  constructor(message?: string, moreInfo?: string) {
    super(
      message || 'Internal Server Error',
      '[The server encountered an internal error or misconfiguration, ' +
        'check if the host you are trying to reach is available and online]',
      moreInfo ||
        'https://blog.airbrake.io/blog/http-errors/500-internal-server-error',
    );
  }
}

export class BadGatewayError extends InternalServerError {
  status = 502;
  constructor(message?: string) {
    super(
      message || 'Bad Gateway',
      'https://blog.airbrake.io/blog/http-errors/502-bad-gateway-error',
    );
  }
}
