import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { NodeModule } from 'http/example/node.module';
import { load } from 'config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      ignoreEnvFile: true, // the env file is already loaded in main.ts
      load,
    }),
    MongooseModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        uri: `${configService.get('DB_HOST')}:${configService.get(
          'DB_PORT',
        )}/${configService.get('DB_NAME')}`,
      }),
      inject: [ConfigService],
    }),
    NodeModule,
  ],
  providers: [ConfigService],
})
export class AppModule {}
