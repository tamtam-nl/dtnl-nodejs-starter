import { seeder } from 'nestjs-seeder';
import { MongooseModule } from '@nestjs/mongoose';
import { Node, NodeSchema } from 'http/example/schemas/node.schema';
import { NodesSeeder } from 'seeders/nodes-seeder';
import { ConfigService } from '@nestjs/config';

seeder({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: (configService: ConfigService) => ({
        uri: `${configService.get('DB_HOST')}:${configService.get(
          'DB_PORT',
        )}/${configService.get('DB_NAME')}`,
      }),
      inject: [ConfigService],
    }),
    MongooseModule.forFeature([{ name: Node.name, schema: NodeSchema }]),
  ],
}).run([NodesSeeder]);
