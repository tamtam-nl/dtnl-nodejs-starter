import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { NodeService } from './node.service';
import { CreateNodeDto } from './dto/create-node.dto';
import { Node } from './schemas/node.schema';
import { Response } from '../../util/response';

@Controller('/examples')
export class NodeController {
  constructor(
    private readonly nodeService: NodeService,
    private readonly response: Response,
  ) {}

  @Post()
  async create(@Body() createNodeDto: CreateNodeDto) {
    const createdNode: Node = await this.nodeService.create(createNodeDto);
    return this.response.format({ createNodeDto }, createdNode);
  }

  @Get()
  async findAll(): Promise<Node[]> {
    const nodes: Node[] = await this.nodeService.findAll();
    return this.response.format('none', nodes);
  }

  @Get(':id')
  async find(@Param('id') id: string): Promise<Node> {
    const node: Node = await this.nodeService.findOne(id);
    return this.response.format({ id }, node);
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    const deletedNode = await this.nodeService.delete(id);
    return this.response.format({ id }, deletedNode);
  }
}
