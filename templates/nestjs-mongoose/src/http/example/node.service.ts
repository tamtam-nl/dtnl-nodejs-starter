import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Node } from './schemas/node.schema';
import { CreateNodeDto } from './dto/create-node.dto';

@Injectable()
export class NodeService {
  constructor(
    @InjectModel(Node.name) private readonly nodeModel: Model<Node>,
  ) {}

  async create(createNodeDto: CreateNodeDto): Promise<Node> {
    return await this.nodeModel.create(createNodeDto);
  }

  async findAll(): Promise<Node[]> {
    return this.nodeModel.find().exec();
  }

  async findOne(id: string): Promise<Node> {
    return this.nodeModel.findOne({ _id: id }).exec();
  }

  async delete(id: string) {
    return await this.nodeModel.findByIdAndRemove({ _id: id }).exec();
  }
}
