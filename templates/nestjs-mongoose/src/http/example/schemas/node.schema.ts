import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Factory } from 'nestjs-seeder';

export type NodeDocument = Node & Document;

@Schema()
export class Node {
  @Factory((faker) => faker.random.arrayElement(['example', 'node']))
  @Prop({ required: true })
  name: string;
}

export const NodeSchema = SchemaFactory.createForClass(Node);
