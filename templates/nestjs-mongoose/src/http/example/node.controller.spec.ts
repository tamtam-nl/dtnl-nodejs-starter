import { Test, TestingModule } from '@nestjs/testing';
import { NodeController } from './node.controller';
import { CreateNodeDto } from './dto/create-node.dto';
import { NodeService } from './node.service';
import { Response } from '../../util/response';

describe('Node Controller', () => {
  let controller: NodeController;
  let service: NodeService;
  const createNodeDto: CreateNodeDto = {
    name: 'Node #1',
  };

  const mockNode = {
    name: 'Node #1',
    _id: 'an id',
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NodeController],
      providers: [
        NodeService,
        Response,
        {
          provide: NodeService,
          useValue: {
            findAll: jest.fn().mockResolvedValue([
              {
                name: 'Node #1',
              },
              {
                name: 'Node #2',
              },
              {
                name: 'Node #3',
              },
            ]),
            create: jest.fn().mockResolvedValue(createNodeDto),
          },
        },
      ],
    }).compile();

    controller = module.get<NodeController>(NodeController);
    service = module.get<NodeService>(NodeService);
  });

  describe('create()', () => {
    it('should create a new node', async () => {
      const createSpy = jest
        .spyOn(service, 'create')
        .mockResolvedValueOnce(mockNode);

      await controller.create(createNodeDto);
      expect(createSpy).toHaveBeenCalledWith(createNodeDto);
    });
  });

  describe('findAll()', () => {
    it('should return an array of nodes', async () => {
      expect(controller.findAll()).resolves.toEqual({
        metadata: 'none',
        results: [
          { name: 'Node #1' },
          { name: 'Node #2' },
          { name: 'Node #3' },
        ],
      });
      expect(service.findAll).toHaveBeenCalled();
    });
  });
});
