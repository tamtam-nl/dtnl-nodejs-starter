import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { NodeService } from './node.service';
import { Node } from './schemas/node.schema';

const mockNode = {
  name: 'Node #1',
};

describe('NodeService', () => {
  let service: NodeService;
  let model: Model<Node>;

  const nodeArray = [
    {
      name: 'Node #1',
    },
    {
      name: 'Node #2',
    },
  ];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NodeService,
        {
          provide: getModelToken('Node'),
          useValue: {
            new: jest.fn().mockResolvedValue(mockNode),
            constructor: jest.fn().mockResolvedValue(mockNode),
            find: jest.fn(),
            create: jest.fn(),
            exec: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<NodeService>(NodeService);
    model = module.get<Model<Node>>(getModelToken('Node'));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return all nodes', async () => {
    jest.spyOn(model, 'find').mockReturnValue({
      exec: jest.fn().mockResolvedValueOnce(nodeArray),
    } as any);
    const nodes = await service.findAll();
    expect(nodes).toEqual(nodeArray);
  });

  it('should insert a new node', async () => {
    jest.spyOn(model, 'create').mockImplementationOnce(() =>
      Promise.resolve({
        name: 'Node #1',
      }),
    );
    const newNode = await service.create({
      name: 'Node #1',
    });
    expect(newNode).toEqual(mockNode);
  });
});
