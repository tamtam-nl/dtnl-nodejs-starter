import { ExceptionFilter, Catch, ArgumentsHost, Logger } from '@nestjs/common';
import { HttpException, HttpStatus } from '@nestjs/common';
import { getCustomMessage } from 'util/errors';
import { ConfigService } from '@nestjs/config';

@Catch()
export class GlobalExceptionFilter implements ExceptionFilter {
  private getStatusCode = (exception: any): number => {
    return exception instanceof HttpException
      ? exception.getStatus()
      : HttpStatus.INTERNAL_SERVER_ERROR;
  };

  private getErrorMessage = (exception: any): string => {
    return String(exception);
  };

  private getResponseFromExecutionContext(context: ArgumentsHost) {
    switch (context.getType<string>()) {
      case 'http':
        return context.switchToHttp().getResponse();
      // add more cases here (f.e graphql, rpc, etc)
    }
  }

  private readonly logger: Logger;

  constructor(private readonly configService: ConfigService) {
    this.logger = new Logger();
  }

  catch(exception: Error, host: ArgumentsHost) {
    const response = this.getResponseFromExecutionContext(host);
    const code = this.getStatusCode(exception);
    const message = this.getErrorMessage(exception);
    const customError = getCustomMessage(code);

    const devErrorResponse: any = {
      status: code,
      developerMessage: message + customError.devMessage,
      userMessage: {
        code,
        message:
          'This is a message that can be passed along to end-users, if needed.',
      },
      moreInfo: customError.moreInfo,
    };

    this.logger.log(
      `[request method: ${response.method} request url${response.url}]`,
      JSON.stringify(devErrorResponse),
    );

    response
      .status(code)
      .json(
        this.configService.get<string>('app.env') === 'development'
          ? devErrorResponse
          : devErrorResponse.userMessage,
      );
  }
}
