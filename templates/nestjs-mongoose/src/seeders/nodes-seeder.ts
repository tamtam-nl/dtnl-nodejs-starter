import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Seeder, DataFactory } from 'nestjs-seeder';
import { Model } from 'mongoose';
import { Node } from 'http/example/schemas/node.schema';

@Injectable()
export class NodesSeeder implements Seeder {
  constructor(@InjectModel(Node.name) private readonly user: Model<Node>) {}

  async seed(): Promise<any> {
    // Generate 10 nodes.
    const users = DataFactory.createForClass(Node).generate(10);

    // Insert into the database.
    return this.user.insertMany(users);
  }

  async drop(): Promise<any> {
    return this.user.deleteMany({});
  }
}
