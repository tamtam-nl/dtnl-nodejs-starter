export const appConfig = {
  version: 'v1',
  env: process.env.NODE_ENV || 'development',
  port: process.env.NODE_PORT || 3000,
};
