export const dbConfig = {
  host: process.env.DB_HOST || 'mongodb://127.0.0.1',
  name: process.env.DB_NAME || 'test',
  port: process.env.DB_PORT || 27017,
};
