import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { GlobalExceptionFilter } from 'common/middleware/exceptionHandler';
import { ConfigService } from '@nestjs/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });

  const config = new DocumentBuilder()
    .setTitle('Starter example')
    .setDescription('The Starter example description')
    .setVersion('1.0')
    .addTag('example')
    .build();

  app.setGlobalPrefix(app.get(ConfigService).get('app.version'));
  app.useGlobalFilters(new GlobalExceptionFilter(app.get(ConfigService)));

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(app.get(ConfigService).get('app.port'));
}
bootstrap();
