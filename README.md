# DEPT Node.js Template

Hey there! This is a collection of DEPT Node.js project templates. This repo is designed to provide a set of usable project skeletons with all of our linter rules, tooling, preferences etc. already in place, so you can spend more time building cool stuff, and less time setting it all up. It is provided in a **fully documented** state, with plenty of code comments, so that people who are unfamiliar with Node.js or how we work at DEPT can see exactly what everything does.

This codebase is offered as a _living standard_. It is designed for debate and discussion. If you think something could be better, or you think it needs something extra, then feel free to make the change! Submit a pull request by following our [Contribution guide](CONTRIBUTING.md) so we can discuss and vote on your change together as a team.

## Quick Start

Pick the template project from the `templates` folder that matches your needs most and copy paste it as a new project. You can then follow the instructions in the `README.md` file in that folder to get started.

## Development

This project requires **at least Node.js v14**. Always use the [latest LTS version](https://nodejs.org/en/about/releases/) of Node.js when you can!

Version management configuration for Node.js is provided for [`volta`](https://volta.sh/). We recommend you have this installed to automatically switch between Node.js versions when you enter one of our project directories. This allows for more deterministic and reproducible builds, which makes debugging easier.

You use `volta` to configure the project to use the latest LTS version of Node.js by running:

```bash
volta pin node@lts
```

You can run this command again to update the version.

### Versioning

This repo uses [Semantic Versioning](https://semver.org/) (often referred to as _semver_).

## Contributing

Thinking of contributing to this repo? Awesome! 🚀

Please follow our [Contribution guide](CONTRIBUTING.md) and follow the DEPT [Code of Conduct](https://sites.google.com/deptagency.com/employeehandbooknl/home/house-rules/office-related/code-of-conduct).

## Further reading

Other useful resources for Node.js development.

### Node.js

- [Node Best Practices](https://github.com/goldbergyoni/nodebestpractices)
- [awesome-nodejs](https://github.com/sindresorhus/awesome-nodejs)
- [awesome-nodejs-security](https://github.com/lirantal/awesome-nodejs-security)

### TypeScript

- [TypeScript Deep Dive](https://basarat.gitbook.io/typescript/)
- [TypeScript Node.js Cheatsheet](https://github.com/typescript-cheatsheets/node)
- [TypeScript Utilities Cheatsheet](https://github.com/typescript-cheatsheets/utilities)

### Miscellaneous

- [Azure Cloud Design Patterns](https://docs.microsoft.com/en-us/azure/architecture/patterns/)

## Support

Feeling lost? Come and find us in the [`#nodejs-collective`](https://deptagency.slack.com/archives/CG2UXMVB4) channel on Slack for some friendly advice!
